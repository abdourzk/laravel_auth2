<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{
    public function register(Request $request)
    {

        return User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);
    }
    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response([
                'message' => 'Invalid credentials!'
            ], Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();

        $token = $user->createToken('token')->plainTextToken;
        $cookie = cookie('jwt', $token, 60 * 4); // 1 day

        return response([
            'message' => $token
        ])->withCookie($cookie);
    }

    public function filter(Request $req)
    {
        try{
            $user = DB::table('formatrice')->where([
                
                ['Pseudo','=',$req->Pseudo],
                ['password','=',$req->password]
            ])->get();
            return [
                "etat"=>1,
                "data"=>$user,
            ];
           
        }
       catch(\Throwable $th)
       {
            return $th->getMessage();
       }
    }
}
